# Racket Pet

<p align="center">
    <a href="https://gitlab.com/xgqt/rktpet/pipelines">
        <img src="https://gitlab.com/xgqt/rktpet/badges/master/pipeline.svg">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/badge/license-GPLv3-blue.svg">
    </a>
</p>


GTK Tamagotchi in Racket


# Disclaimer

Work in Progress


# License

GPL v3
